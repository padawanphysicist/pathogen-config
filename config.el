(set-face-attribute 'default nil
                    :family "Noto Sans Mono"
                    :height 150
                    :weight 'normal
                    :width 'normal)

(use-package ef-themes
  :config
  (progn
    ;; Disable all themes
    (dolist (i custom-enabled-themes)
      (disable-theme i))
    ;; Load "default" theme
    (load-theme 'ef-day t nil)))

;; M-x nerd-icons-install-fonts
(use-package nerd-icons)
(use-package doom-modeline
  :config
  ;; How tall the mode-line should be. It's only respected in GUI.
  ;; If the actual char height is larger, it respects the actual height.
  (setq doom-modeline-height 36)
  :init (doom-modeline-mode 1))

(use-package multiple-cursors
  :config
  (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this))

(use-package company :config
  (add-hook 'after-init-hook 'global-company-mode))

(use-package flycheck
  :config
  (add-hook 'prog-mode-hook 'flycheck-mode))

(defun vct/eval-region-dwim ()
  (interactive)
  (eval-region (region-beginning) (region-end))
  (message "Region evaluated"))
(define-key lisp-mode-map (kbd "<C-return>") 'vct/eval-region-dwim)

(load (expand-file-name "~/.roswell/helper.el"))
(setq inferior-lisp-program "ros -Q run")
;;(use-package sly)

(use-package markdown-mode+)

(use-package dockerfile-mode)
(use-package docker-compose-mode)

(use-package python-black
  :ensure t
  :bind (("C-c b" . python-black-buffer)))

(use-package pyvenv
  :ensure t
  :config
  (pyvenv-mode 1))

(use-package anaconda-mode
  :ensure t
  :bind (("C-c C-x" . next-error))
  :config
  (require 'pyvenv)
  (add-hook 'python-mode-hook 'anaconda-mode)
  (setq python-shell-interpreter "ipython"
        python-shell-interpreter-args "-i --simple-prompt")
  ;; (add-hook 'flycheck-mode-hook #'flycheck-virtualenv-setup)
  )

(use-package company-anaconda
  :ensure t
  :config
  (eval-after-load "company"
   '(add-to-list 'company-backends '(company-anaconda :with company-capf))))

(use-package highlight-indent-guides
  :ensure t
  :config
  (add-hook 'python-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-method 'character))

;; (use-package elpy
;;   :config
;;   (elpy-enable)
;;   (setq python-shell-interpreter "ipython"
;;         python-shell-interpreter-args "-i --simple-prompt"))

(use-package geiser-guile
  :config
  (setq geiser-guile-binary "/home/vct/.local/bin/guile"))

(use-package google-translate
  :config
  (require 'google-translate-smooth-ui)
  (global-set-key "\C-ct" 'google-translate-smooth-translate))

(use-package hyperbole
  :config
  (hyperbole-mode 1))

;;(add-hook 'org-mode-hook 'org-indent-mode)
(setq org-startup-indented t)

(setq org-src-preserve-indentation t)

(setq org-confirm-babel-evaluate nil)

(setq org-src-tab-acts-natively t)

(setq org-src-preserve-indentation t)

(org-babel-do-load-languages
  'org-babel-load-languages
  '((emacs-lisp . t)
    (ruby . t)
    (shell . t)
    (python . t)
    ;; (R . t)
    (scheme . t)
    (sql . t)))

(setq org-babel-default-header-args:ruby '((:session . "ruby")
                                           (:noweb . "yes")
                                           (:results . "output")))

(setq org-babel-default-header-args:R '((:session . "*R*")
                                        (:noweb . "yes")
                                        (:results . "output")
                                        (:eval . "never-export")))

;; Ob-sagemath supports only evaluating with a session.
(setq org-babel-default-header-args:sage '((:session . t)
                                           (:noweb . "yes")
                                           (:results . "output")))

(use-package org-roam
  :custom
  (org-roam-directory (file-truename "~/Dropbox/Roam") "Location of Org-roam files.")
  (org-roam-graph-viewer (executable-find "firefox"))
  (org-roam-capture-templates
   '(("d" "default" plain "%?"
      :if-new
      (file+head "%<%Y%m%d%H%M%S>.org"
                 "#+title: ${title}
#+date: %U
#+filetags: 

")
      :unnarrowed t)))
  :config
  (org-roam-db-autosync-mode)
  :bind
  (("C-c n f" . org-roam-node-find)
   ("C-c n g" . org-roam-graph)))
